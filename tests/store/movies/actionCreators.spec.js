/* globals describe, it */

import { expect } from 'chai';

import * as actionCreators from '../../../client/src/store/movies/actionCreators';
import * as actionTypes from '../../../client/src/store/movies/actionTypes';

describe('Movie Action Creators', () => {
    it('Should create load movie actions', () => {
        const result = actionCreators.loadMovies();

        expect(result.type).to.equal(actionTypes.LOAD_MOVIES);
    });

    it('Should create movies loaded actions', () => {
        const list = [1, 2, 3, 4];
        const result = actionCreators.moviesLoaded(list);

        expect(result.type).to.equal(actionTypes.MOVIES_LOADED);
        expect(result.payload).to.deep.equal(list);
    });

    it('Should create movie load error actions', () => {
        const err = new Error('Test error');
        const result = actionCreators.movieLoadError(err);

        expect(result.type).to.equal(actionTypes.MOVIE_LOAD_ERROR);
        expect(result.payload).to.deep.equal(err);
        expect(result.error).to.be.ok;
    });

    it('Should create set sort order actions', () => {
        const sortField = 'TestField';
        const sortDirection = 'TestDirection';
        const result = actionCreators.setSortOrder({ sortField, sortDirection });

        expect(result.type).to.equal(actionTypes.SET_SORT_ORDER);
        expect(result.payload).to.deep.equal({ sortField, sortDirection });
    });
});
