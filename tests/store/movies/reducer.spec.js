/* globals describe, it */

import { expect } from 'chai';

import reducer from '../../../client/src/store/movies/reducer';
import * as actionCreators from '../../../client/src/store/movies/actionCreators';

describe('Movie Reducer', () => {
    it('Should set the movie loading state', () => {
        const result = reducer(undefined, actionCreators.loadMovies());

        expect(result.get('loading')).to.be.ok;
    });

    it('Should set the movie list when loaded', () => {
        const movies = [1, 2, 3, 4];
        const result = reducer(undefined, actionCreators.moviesLoaded(movies));

        expect(result.get('movies').toJS()).to.deep.equal(movies);
    });

    it('Should set an error when movie load fails', () => {
        const err = new Error('Test Error');
        const result = reducer(undefined, actionCreators.movieLoadError(err));

        expect(result.get('error')).to.deep.equal(err);
    });

    it('Should set the movie sort order', () => {
        const sortField = 'TestField';
        const sortDirection = 'TestDirection';
        const result = reducer(undefined, actionCreators.setSortOrder({ sortField, sortDirection }));

        expect(result.get('sortField')).to.equal(sortField);
        expect(result.get('sortDirection')).to.equal(sortDirection);
    });
});
