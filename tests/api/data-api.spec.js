/* globals describe, it, beforeEach, afterEach */

import 'babel-polyfill';

import { expect } from 'chai';
import { sandbox } from 'sinon';

import axios from 'axios';

import { getMovieData } from '../../client/src/api/data-api';

describe('Data API', () => {
    const testData = {
        data: {
            test: 'test',
        },
    };

    let testSandbox;
    let getStub;

    beforeEach(() => {
        testSandbox = sandbox.create();

        getStub = testSandbox.stub(axios, 'get').callsFake(() => {
            const promise = new Promise((resolve) => {
                resolve(testData);
            });

            return promise;
        });
    });

    afterEach(() => {
        testSandbox.restore();
    });

    it('Should make a GET request for data', () => {
        getMovieData();

        expect(getStub.calledWith('data.json')).to.be.ok;
    });

    it('Should return the loaded data', (done) => {
        getMovieData().then((result) => {
            expect(result).to.deep.equal(testData.data);
            done();
        });
    });
});
