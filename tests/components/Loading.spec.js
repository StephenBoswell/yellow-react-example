/* globals describe, it */

import React from 'react';

import { expect } from 'chai';
import { shallow } from 'enzyme';

import Loading from '../../client/src/components/Loading/Loading';

describe('Loading', () => {
    it('Should display a loading indicator', () => {
        const loading = shallow(<Loading />);

        expect(loading.find('.loading')).to.have.length(1);
    });
});
