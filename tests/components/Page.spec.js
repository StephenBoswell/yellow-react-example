/* globals describe, it */

import React from 'react';

import { expect } from 'chai';
import { shallow } from 'enzyme';

import Page from '../../client/src/components/Page/Page';
import PageHeader from '../../client/src/components/PageHeader/PageHeader';
import MovieTableContainer from '../../client/src/components/MovieTableContainer/MovieTableContainer';

describe('Page', () => {
    it('Should contain a page header', () => {
        const page = shallow(<Page />);

        expect(page.find(PageHeader)).to.have.length(1);
    });

    it('Should contain a movie table', () => {
        const page = shallow(<Page />);

        expect(page.find(MovieTableContainer)).to.have.length(1);
    });
});
