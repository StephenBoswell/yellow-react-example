/* globals describe, it, beforeEach */

import 'babel-polyfill';

import React from 'react';

import { expect } from 'chai';
import { shallow } from 'enzyme';
import { spy } from 'sinon';
import faker from 'faker';

import MovieTable from '../../client/src/components/MovieTable/MovieTable';

describe('MovieTable', () => {
    const testMovies = [];

    for (let i = 0; i < 10; i += 1) {
        testMovies.push({
            rank: i + 1,
            title: faker.lorem.sentence(),
            year: faker.random.number(),
            director: `${faker.name.firstName()} ${faker.name.lastName()}`,
        });
    }

    let setSort;

    beforeEach(() => {
        setSort = spy();
    });

    it('Should display the movie information', () => {
        const movieTable = shallow(<MovieTable
            movies={testMovies}
            sortDirection="forwards"
            sortField="rank"
            setSort={setSort}
        />);

        testMovies.forEach((movie) => {
            expect(movieTable.contains(movie.rank)).to.be.ok;
            expect(movieTable.contains(movie.title)).to.be.ok;
            expect(movieTable.contains(movie.year)).to.be.ok;
            expect(movieTable.contains(movie.director)).to.be.ok;
        });
    });

    it('Should call the setSort method when sort buttons are clicked', () => {
        const movieTable = shallow(<MovieTable
            movies={testMovies}
            sortDirection="forwards"
            sortField="rank"
            setSort={setSort}
        />);

        movieTable.find('button')
            .first()
            .simulate('click');

        expect(setSort.called).to.be.ok;
    });
});
