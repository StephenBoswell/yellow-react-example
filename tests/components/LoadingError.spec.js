/* globals describe, it */

import React from 'react';

import { expect } from 'chai';
import { shallow } from 'enzyme';

import LoadingError from '../../client/src/components/LoadingError/LoadingError';

describe('LoadingError', () => {
    it('Should display an error messages', () => {
        const loadingError = shallow(<LoadingError />);

        expect(loadingError.contains('A loading error occurred.')).to.be.ok;
    });
});
