/* globals describe, it */

import React from 'react';

import { expect } from 'chai';
import { shallow } from 'enzyme';

import PageHeader from '../../client/src/components/PageHeader/PageHeader';

describe('PageHeader', () => {
    it('Should display a page header', () => {
        const pageHeader = shallow(<PageHeader />);

        expect(pageHeader.find('h1')).to.have.length(1);
        expect(pageHeader.contains('Top Movies')).to.be.ok;
    });
});
