import axios from 'axios';

// Would normally have multiple exports here, but because this example only have 1, so...
// eslint-disable-next-line import/prefer-default-export
export const getMovieData = async () => {
    const res = await axios.get('data.json');

    return res.data;
};
