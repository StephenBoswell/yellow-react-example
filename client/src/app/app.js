import React from 'react';
import { render } from 'react-dom';

import { Provider } from 'react-redux';

import store from '../store/store';

import Page from '../components/Page/Page';

const app = () => {
    const appContainer = document.getElementById('app-container');

    render(
        <Provider store={store}>
            <Page />
        </Provider>,
        appContainer,
    );
};

export default app;
