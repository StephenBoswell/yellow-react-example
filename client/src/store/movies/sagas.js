import { takeLatest, call, put } from 'redux-saga/effects';

import { LOAD_MOVIES } from './actionTypes';
import { moviesLoaded, movieLoadError } from './actionCreators';

import { getMovieData } from '../../api/data-api';

const loadMovies = function* () {
    try {
        const data = yield call(getMovieData);

        yield put(moviesLoaded(data));
    } catch (error) {
        yield put(movieLoadError(error));
    }
};

export default function* () {
    yield takeLatest(LOAD_MOVIES, loadMovies);
}
