const express = require('express');
const path = require('path');

const data = require('./data.json');

const app = express();

app.use('/static', express.static(path.join(__dirname, 'client/dist')));

app.get('/data.json', (req, res) => {
    setTimeout(() => {
        res.json(data);
    }, 2000);
});

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '/html/index.html'));
});

app.listen(3000);

console.log('Yellow React Example listing on Port 3000. Please visit http://localhost:3000');
